package org.aibles.okr.entity;

import java.sql.Date;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Data;

@Data
@Table(name = "commit")
public class Commit {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "id")
  private Long id;

  @Column(name = "messenger", unique = true, length = 256)
  private String messenger;

  @Column(name = "createdAt")
  private Date createdAt;

  @Column(name = "progress")
  private Float progress;


  @Column(name = "keyresults_id")
  private Long keyresultsId;

}
