package org.aibles.okr.entity;


import com.fasterxml.jackson.annotation.JsonManagedReference;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import lombok.Data;

@Data
@Table(name = "user")
public class User {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "id")
  private Long id;

  @Column(name = "email", unique = true, length = 100)
  private String email;

  @Column(name = "password", unique = true, length = 20)
  private String password;

  @Column(name = "firstname", unique = true, length = 20)
  private String firstname;

  @Column(name = "lastname", unique = true, length = 20)
  private String lastname;

  @Column(name = "number_phone", unique = true, length = 15)
  private String numberPhone;

  @Column(name = "date_of_birth")
  private Date dateOfBirth;

  @Column(name = "address", unique = true, length = 256)
  private String address;

  @Column(name = "created_at")
  private Date createdAt;

  @Column(name = "updated_at")
  private Date updatedAt;

  @OneToMany(mappedBy = "user", cascade = CascadeType.ALL)
  @JsonManagedReference
  private List<Objective> objectiveList;




}
