package org.aibles.okr.entity;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Data;


@Data
@Table(name = "keyresults")
public class KeyResults {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "id")
  private Long id;

  @Column(name = "name", unique = true, length = 256)
  private String name;

  @Column(name = "description", unique = true, length = 256)
  private String description;

  @Column(name = "deadline")
  private Date deadline;

  @Column(name = "progress")
  private Float progress;


  @Column(name = "status", unique = true, length = 256)
  private String status;

  @Column(name = "objective_id")
  private Long objectiveId;

  @Column(name = "created_at")
  private Date createdAt;

  @Column(name = "updated_at")
  private Date updatedAt;

}
