package org.aibles.okr.entity;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Data;


@Data
@Table(name = "objective")
public class Objective {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "id")
  private Long id;

  @Column(name = "name", unique = true, length = 100)
  private String name;

  @Column(name = "type", unique = true, length = 256)
  private String type;

  @Column(name = "description", unique = true, length = 256)
  private String description;

  @Column(name = "deadline")
  private Date deadline;

  @Column(name = "progress")
  private Float progress;

  @Column(name = "user_id")
  private Long userId;

  @Column(name = "created_at")
  private Date createdAt;

  @Column(name = "updated_at")
  private Date updatedAt;


}
