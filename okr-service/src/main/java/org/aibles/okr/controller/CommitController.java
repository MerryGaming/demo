package org.aibles.okr.controller;

import lombok.extern.slf4j.Slf4j;
import org.aibles.okr.dto.request.history.CreateCommit;
import org.aibles.okr.dto.response.CommitResponse;
import org.aibles.okr.service.CommitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/commits")
@Slf4j
public class CommitController {

  private final CommitService service;

  @Autowired
  public CommitController(CommitService service) {
    this.service = service;
  }

  @PostMapping
  @ResponseStatus(HttpStatus.CREATED)
  public CommitResponse created(@RequestBody CreateCommit createCommit) {
    return service.created(createCommit);
  }

//  @DeleteMapping("/{id}")
//  @ResponseStatus(HttpStatus.OK)
//  public String deleteById(@PathVariable("id") long id) {
//
//    historyService.delete(id);
//    return "Successful delete";
//  }
//
//  @GetMapping
//  @ResponseStatus(HttpStatus.OK)
//  public List<History> list() {
//    return historyService.list();
//  }
//
//
//  @PutMapping("{id}")
//  @ResponseStatus(HttpStatus.OK)
//  public HistoryDto update(@PathVariable("id") long id, @RequestBody @Valid HistoryDto historyDto) {
//    return historyService.update(id, historyDto);
//  }
}
