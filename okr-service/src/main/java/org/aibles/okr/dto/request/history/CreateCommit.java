package org.aibles.okr.dto.request.history;

import java.sql.Date;
import javax.validation.constraints.NotBlank;
import lombok.Data;
import org.aibles.okr.entity.Commit;

@Data
public class CreateCommit {

  @NotBlank
  private String messenger;
  private Date createdAt;
  private float progress;
  private long keyresultsId;

  public CreateCommit() {
  }

  public String getMessenger() {
    return messenger;
  }

  public void setMessenger(String messenger) {
    this.messenger = messenger;
  }

  public float getProgress() {
    return progress;
  }

  public void setProgress(float progress) {
    this.progress = progress;
  }

  public Date getCreatedAt() {
    return createdAt;
  }

  public void setCreatedAt(Date createdAt) {
    this.createdAt = createdAt;
  }

  public long getKeyresultsId() {
    return keyresultsId;
  }

  public void setKeyresultsId(long keyresultsId) {
    this.keyresultsId = keyresultsId;
  }

  public Commit toCommit() {
    Commit commit = new Commit();
    commit.setMessenger(this.getMessenger());
    commit.setProgress(this.getProgress());
    commit.setCreatedAt(this.getCreatedAt());
    commit.setKeyresultsId(this.getKeyresultsId());
    return commit;
  }
}
