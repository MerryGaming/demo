package org.aibles.okr.dto.response;

import java.sql.Date;
import org.aibles.okr.entity.Commit;

public class CommitResponse {

  private long id;
  private String messenger;
  private Date createdAt;
  private float progress;
  private long keyresultsId;

  public CommitResponse() {
  }

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public String getMessenger() {
    return messenger;
  }

  public void setMessenger(String messenger) {
    this.messenger = messenger;
  }

  public Date getCreatedAt() {
    return createdAt;
  }

  public void setCreatedAt(Date createdAt) {
    this.createdAt = createdAt;
  }

  public float getProgress() {
    return progress;
  }

  public void setProgress(float progress) {
    this.progress = progress;
  }

  public long getKeyresultsId() {
    return keyresultsId;
  }

  public void setKeyresultsId(long keyresultsId) {
    this.keyresultsId = keyresultsId;
  }

  public static CommitResponse from(Commit commit) {
    CommitResponse response = new CommitResponse();
    response.setId(commit.getId());
    response.setMessenger(commit.getMessenger());
    response.setProgress(commit.getProgress());
    response.setCreatedAt(commit.getCreatedAt());
    response.setKeyresultsId(commit.getKeyresultsId());
    return response;
  }
}
