package org.aibles.okr.service;

import org.aibles.okr.dto.request.user.CreateUser;
import org.aibles.okr.dto.request.user.UpdateUser;
import org.aibles.okr.dto.response.UserResponse;

public interface UserService {

  UserResponse created(CreateUser createUser);

//  void delete(long id);
//
//  List<User> list();

  UserResponse update(long id, UpdateUser updateUser);
}
