package org.aibles.okr.service.impl;

import lombok.extern.slf4j.Slf4j;
import org.aibles.okr.dto.request.history.CreateCommit;
import org.aibles.okr.dto.response.CommitResponse;
import org.aibles.okr.entity.Commit;
import org.aibles.okr.repository.CommitRepository;
import org.aibles.okr.service.CommitService;

@Slf4j
public class CommitServiceImpl implements CommitService {

  private final CommitRepository repository;


  public CommitServiceImpl(CommitRepository repository) {
    this.repository = repository;
  }

  @Override
  public CommitResponse created(CreateCommit createCommit) {
    log.info("(Create)create commit: {}", createCommit);
    Commit commit = createCommit.toCommit();
    Commit create = repository.save(commit);
    CommitResponse commitCreated = CommitResponse.from(create);
    return commitCreated;
  }

//  @Override
//  public void delete(long id) {
//    log.info("Delete history by id");
//    History history =
//        repository
//            .findById(id)
//            .orElseThrow(
//                () -> {
//                  throw new InternalServerException(id);
//                }
//            );
//    repository.delete(history);
//  }
//
//  @Override
//  @Transactional
//  public List<History> list() {
//    log.info("List history");
//    List<History> historyList = repository.findAll();
//    return historyList;
//  }

//  @Override
//  public HistoryResponse update(long id, UpdateHistory updateHistory) {
//    log.info("(Update) Update worker by id");
//    History historyCheck =
//        repository
//            .findById(id)
//            .orElseThrow(
//                () -> {
//                  throw new ResourceNotFoundException(id);
//                });
//    History history = updateHistory.toHistory();
//    history.setId(historyCheck.getId());
//    History update = repository.save(history);
//    Optional.of(update)
//        .orElseThrow(
//            () -> {
//              throw new InternalServerException(id);
//            });
//    HistoryResponse historyUpdated = HistoryResponse.from(update);
//    return historyUpdated;
//  }
}
