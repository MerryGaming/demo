package org.aibles.okr.service;

import org.aibles.okr.dto.request.history.CreateCommit;
import org.aibles.okr.dto.response.CommitResponse;


public interface CommitService {

  CommitResponse created(CreateCommit createCommit);

//  void delete(long id);
//
//  List<History> list();

  //HistoryResponse update(long id, UpdateHistory updateHistory);

}
